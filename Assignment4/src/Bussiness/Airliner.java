/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;

import java.util.Date;

/**
 *
 * @author don
 */
public class Airliner {
    private String flightName;
    private String flightnumber;
    private String departureloc;
    private String arivvalloc;
    private String time;
    private int price;
    private String dateofflight;

   public Airliner(){
       
   }
    
     public Airliner() {
    }

    public Airliner(String flightName, String flightnumber, String departureloc, String arivvalloc, String time, int price, String dateofflight) {
       this.flightName = flightName;
       this.flightnumber = flightnumber;
       this.departureloc = departureloc;
       this.arivvalloc = arivvalloc;
       this.time = time;
       this.price=price;
       this.dateofflight = dateofflight;
    }

    
    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public String getFlightnumber() {
        return flightnumber;
    }

    public void setFlightnumber(String flightnumber) {
        this.flightnumber = flightnumber;
    }

    public String getDepartureloc() {
        return departureloc;
    }

    public void setDepartureloc(String departureloc) {
        this.departureloc = departureloc;
    }

    public String getArivvalloc() {
        return arivvalloc;
    }

    public void setArivvalloc(String arivvalloc) {
        this.arivvalloc = arivvalloc;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDateofflight() {
        return dateofflight;
    }

    public void setDateofflight(String dateofflight) {
        this.dateofflight = dateofflight;
    }
    
     @Override
    public String toString(){
        return flightName;
    }
    
    

    
}


