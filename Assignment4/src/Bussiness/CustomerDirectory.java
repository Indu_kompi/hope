/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author don
 */
public class CustomerDirectory {
    private List<String> customerList;
    
    public CustomerDirectory(){
        customerList = new ArrayList<>();
    }

    public List<String> getCustomerList() {
        return customerList;
    }

    public void setCustomerList(List<String> customerList) {
        this.customerList = customerList;
    }

    
}
